<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Breadcrumbs;

use app\widgets\Alert;
use app\widgets\Menu;
use app\themes\stisla\StislaAsset;

use mdm\admin\components\MenuHelper;

StislaAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?> - <?=Yii::$app->name?></title>
  <link rel="shortcut icon" href="<?= Url::base(true); ?>/themes/stisla/assets/syams-icon.png" type="image/x-icon" />
  <?php $this->head() ?>
</head>

<body> <!--class="sidebar-mini"-->
<?php $this->beginBody() ?>
	<div id="app">
		<div class="main-wrapper">
			<div class="navbar-bg"></div>
			<?= $this->render("top_nav") ?>
			<div class="main-sidebar">
			<?= $this->render("side_menu") ?>
			</div>

			<!-- Main Content -->
			<div class="main-content">
				<section class="section">
					<div class="section-body">
						<div class="card card-secondary">
							<div class="card-header">
								<h4>
									<?php
										if (isset($this->params['pageIcon'])) {
											echo '<i class="'.$this->params['pageIcon'].'"></i>&nbsp;';
										}
										$title = $this->title;
										if (isset($this->params['pageHeader'])) {
											$title = $this->params['pageHeader'];
										}
										echo Html::encode($title);
									?>
								</h4>
								<?php
								if (isset($this->params['pageActionButton'])) {
									echo '
										<div class="card-header-action">';
										foreach ($this->params['pageActionButton'] as $row) {
											echo $row;
										}
									echo '
									</div>
									';
								}
								?>
							</div>
							<div class="card-body">
									<?= Alert::widget() ?>
									<?= $content ?>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
