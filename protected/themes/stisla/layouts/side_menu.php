<?php
use yii\helpers\Url;
use app\widgets\Menu;
use mdm\admin\components\MenuHelper;
?>

<aside id="sidebar-wrapper">
  <div class="sidebar-brand">
	<a href="<?=Yii::$app->homeUrl?>">
		<?=Yii::$app->name?>
	</a>
  </div>
  <div class="sidebar-brand sidebar-brand-sm">
	<a href="<?=Yii::$app->homeUrl?>"><?=strtoupper(substr(Yii::$app->name,0,2))?></a>
  </div>
	<?php
	echo Menu::widget(
		[
			'options' => ['class' => 'sidebar-menu'],
			'linkTemplate' => '<a href="{url}" class="nav-link">{icon} {label}</a>',
			'activeCssClass' => 'active',
			'items' => MenuHelper::getAssignedMenu(
			  Yii::$app->user->id,
			  null,
			  function ($menu) {
				$urlParams = [$menu['route']];
				$json = $menu['params'];
				if(!empty($json)){
				  $param = Json::decode($json, $asArray = true);
				  foreach ($param as $key => $value) {
					  $urlParams[$key] = $value;
				  }
				}
				if($menu['name'] == 'Warehouse' ||
					$menu['name'] == 'Keuangan' ||
					$menu['name'] == 'Laporan Aulia Hospital' ||
					$menu['name'] == 'Farmasi' ||
					$menu['name'] == 'CRM' ||
					$menu['name'] == 'Poliklinik' ||
					$menu['name'] == 'MCU' ||
					$menu['name'] == 'Ruang Bedah' ||
					$menu['name'] == 'Hemodialisa' ||
					$menu['name'] == 'Rehab Medik' ||
					$menu['name'] == 'Ruang Rawat Inap' ||
					$menu['name'] == 'Dokter' ||
					$menu['name'] == 'Ruang Bersalin' ||
					$menu['name'] == 'Radiology' ||
					$menu['name'] == 'Konsultasi Gizi' ||
					$menu['name'] == 'Laboratorium' ||
					$menu['name'] == 'Rekam Medis'
				){
					return [
						'label' => $menu['name'],
						'url' => $urlParams,
						'options' => ['class' => 'nav-item dropdown'],
						'icon' => $menu['data'],
					];
				}else{
					return [
						'label' => $menu['name'],
						'url' => $urlParams,
						'options' => ['class' => 'nav-item dropdown'],
						'icon' => $menu['data'],
						//'items' => $menu['children'],
					];
				}

			  }
			),
		]
	  );
	?>
</aside>
