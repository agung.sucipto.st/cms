<?php
use app\widgets\HeadMenu;
use yii\bootstrap4\Nav;
use kartik\bs4dropdown\Dropdown;
use mdm\admin\components\MenuHelper;
use yii\helpers\Json;
if(isset($this->params['menuid'])){
echo Nav::widget(
[		
	'dropdownClass' => Dropdown::classname(),
	'options' => ['class' => 'navbar-nav mr-auto'],
	'items' => MenuHelper::getAssignedMenu(
	  Yii::$app->user->id,
	  $this->params['menuid'],
	  function ($menu) {
		  $urlParams = [$menu['route']];
			$json = $menu['params'];
			if (!empty($json)) {
			  $param = Json::decode($json, $asArray = true);
			  foreach ($param as $key => $value) {
				$urlParams[$key] = $value;
			  }
			}

		  return [
			  'label' => $menu['name'],
			  'url' => $urlParams,
			  'options' => ['class' => 'nav-item dropdown'],
			  'icon' => $menu['data'],
			 'items' => $menu['children'],
		  ];
		}
	),
]
);
}
?>
