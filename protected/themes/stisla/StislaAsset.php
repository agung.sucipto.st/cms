<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\themes\stisla;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class StislaAsset extends AssetBundle
{
    public $basePath = '@app/themes/stisla';
    public $baseUrl = '@web/themes/stisla';
    public $css = [
        'assets/css/all.min.css',
        'assets/css/style.css',
        'assets/css/custom.css',
        'assets/css/components.css',
    ];
    public $js = [
        'assets/js/jquery.nicescroll.min.js',
        'assets/js/moment.min.js',
        'assets/js/stisla.js',
        'assets/js/scripts.js',
        'assets/js/custom.js',
        'js/yii_overrides.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
		'yii\bootstrap4\BootstrapPluginAsset',
        //'app\themes\stisla\SweetAlertAsset',
    ];
}
