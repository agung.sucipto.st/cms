<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class CoreModulController extends Controller
{
    public function actionIndex()
    {
		$this->view->params['menuid'] = 110561;
        return $this->render('index');
    }
}
