<?php

namespace app\components;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Menu;

class MenuHelper
{
    public static function Menu($array, $parent_id = 0)
    {
        $normal = array();
        $temp_array = array();
        foreach ($array as $element) {
            if ($element->parent == $parent_id) {
                $normal['label'] = $element->name;
                $normal['url'] = array($element->menu_url);
                $normal['icon'] = $element->menu_icon;

                if ($normal['url'] != "#") {
                    $normal['visible'] = Yii::$app->user->can($normal['url'][0]);
                } else {
                    $nromal['visible'] = true;
                }
                $child = self::Menu($array, $element->id);
                if (isset($child)) {
                    $normal['items'] = $child;
                    foreach ($normal['items'] as $row) {
                        if ($row['visible'] == true) {
                            $normal['visible'] = true;
                        }
                    }
                }
                $temp_array[] = $normal;
            }
        }
        return $temp_array;
    }

    public static function MenuManager($array, $parent_id = 0)
    {
        $normal = 0;
        $menu = '';
        foreach ($array as $element) {
            if ($element->parent == $parent_id) {
                $normal++;
                $menu .= '<li class="dd-item dd3-item" data-id="' . $element->id . '"><div class="dd-handle dd3-handle"></div><div class="dd3-content">
				<div class="row">
				<div class="col-sm-6">
                ' . $element->data . ' ' . $element->name . ' <b>[ID '.$element->id.']</b>
				</div>
				<div class="col-sm-6 text-right">';

                $menu .= Html::a('<i class="fa fa-edit"></i>&nbsp;',
                    ['/menu/update', 'id' => $element->id]);
                if (self::MenuChild($element->id) == 0) {
                    $menu .= Html::a('<i class="fa fa-trash"></i>',
                        ['/menu/delete', 'id' => $element->id],
                        [
                            'onClick' => 'return confirm("' . Yii::t('app', 'Are You Sure to delete this Data?') . '");',
                            'data' => [
                                'method' => 'post',
                            ],
                        ]
                    );
                }
                $menu .= '</div></div></div>';
                if (self::MenuManager($array, $element->id) != '') {$menu .= self::MenuManager($array, $element->id);}

                $menu .= '</li>';
            }
        }
        if ($normal > 0) {
            return '<ol class="dd-list">' . $menu . '</ol>';
        } else {
            return '';
        }

    }

    public static function MenuJsonSave($array, $parent = null)
    {
        $order = 0;
        foreach ($array as $element) {

            $order++;
                       if (isset($element->children)) {
                $model = Menu::findOne($element->id);
                $model->order = $order;
                $model->parent = $parent;
                $model->save();
                $child = self::MenuJsonSave($element->children, $element->id);
            } else {
                $model = Menu::findOne($element->id);
                $model->order = $order;
                $model->parent = $parent;
                $model->save();
            }
        }

    }

    public static function MenuChild($id)
    {
        $model = Menu::find()->where(['parent' => $id])->count();
        return $model;
    }

}
