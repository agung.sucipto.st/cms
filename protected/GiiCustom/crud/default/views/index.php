<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\helpers\Url;
use mdm\admin\components\Helper;
use <?= $generator->indexWidgetType === 'grid' ? "kartik\grid\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageIcon'] = 'fa fa-th-large';
$this->params['pageHeader'] = $this->title;
$this->params['pageActionButton'][] = Html::a('<i class="fa fa-search"></i> Cari', '#<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search', ['class' => 'btn btn-warning', 'data-toggle' => 'collapse' , 'aria-controls' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search']);
$this->params['pageActionButton'][] = Helper::checkRoute('create') ? Html::a('<i class="fa fa-plus"></i> Tambah', ['create'], ['class' => 'btn btn-primary']) : null;
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">

<?= $generator->enablePjax ? "    <?php Pjax::begin(); ?>\n" : '' ?>
<?php if(!empty($generator->searchModelClass)): ?>
<?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "" : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
<?php endif; ?>

<?php if ($generator->indexWidgetType === 'grid'): ?>
    <?= "<?= " ?>GridView::widget([
        'dataProvider' => $dataProvider,
        'krajeeDialogSettings' => [ 'overrideYiiConfirm' => false ],
        'layout' => '<div class="row mb-1"><div class="col-sm-6">{summary}</div><div class="col-sm-6 text-right">{toolbar}</div></div>{items}{pager}',
        'exportConfig' => [
            GridView::EXCEL => ['label' => 'Save as EXCEL','filename' => 'List '.<?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>.' '.date('Y-m-d-H:i:s'),],
            GridView::CSV => ['label' => 'Save as CSV', 'filename' => 'List '.<?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>.' '.date('Y-m-d-H:i:s'),],
            GridView::JSON => ['label' => 'Save as JSON', 'filename' => 'List '.<?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>.' '.date('Y-m-d-H:i:s'),],
        ],
        <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
            ['class' => 'kartik\grid\SerialColumn'],
<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
          if (++$count < 6) {
              if($count!=1) {
                echo "            '" . $name . "',\n";
              }
          } else {
              echo "            //'" . $name . "',\n";
          }
    }
} else {
    foreach ($tableSchema->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        if (++$count < 6) {
          if($count!=1) {
              echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
            }
        } else {
            echo "            //'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        }
    }
}
?>
            [
							'class' => 'kartik\grid\ActionColumn',
              'header' => '#',
              'dropdown' => true,
              'dropdownButton' => [
                'class' => 'btn btn-sm btn-primary btn-icon',
                'label' => '<i class="fas fa-chevron-circle-down"></i>',
                'title' => 'Klik Untuk Fungsi'
              ],
							'template' => Helper::filterActionColumn('{view} {update} {delete}'),
							'buttons' => [
								'view' => function ($url, $model) {
									return Html::a('<i class="fa fa-search"></i> Detail',
										Yii::$app->urlManager->createUrl([Yii::$app->controller->uniqueId.'/view', <?= $urlParams ?>]),
										['title' => 'Detail', 'class' => 'btn btn-icon btn-sm btn-success']
									);
								},
								'update' => function ($url, $model) {
									return Html::a('<i class="fa fa-edit"></i> Edit',
										Yii::$app->urlManager->createUrl([Yii::$app->controller->uniqueId.'/update', <?= $urlParams ?>]),
										['title' => 'Edit', 'class' => 'btn btn-icon btn-sm btn-info']
									);
								},
								'delete' => function ($url, $model) {
									  $url = Url::to([Yii::$app->controller->uniqueId.'/delete', <?= $urlParams ?>]);
									  return Html::a('<i class="fa fa-trash"></i> Hapus', $url, [
										  'title'        => 'Hapus',
										  'data-confirm' => 'Apakah Yakin Akan Menghapus Data ini?',
										  'data-method'  => 'post',
										  'class' => 'btn btn-icon btn-sm btn-danger'
									  ]);
								  }
							],
            ],
        ],
    ]); ?>
<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
        },
    ]) ?>
<?php endif; ?>
<?= $generator->enablePjax ? "    <?php Pjax::end(); ?>\n" : '' ?>
</div>
