<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\bootstrap4\Alert;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin(); ?>
    <?= "<?php"?>
    if($model->errors){
      Alert::begin([
          'options' => [
              'class' => 'alert-danger',
          ],
      ]);
      echo $form->errorSummary($model);
      Alert::end();
    }
    <?= "?>"?>

<?php foreach ($generator->getColumnNames() as $attribute) {
    if (in_array($attribute, $safeAttributes)) {
        echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
    }
} ?>
    <div class="form-group">
        <?= "<?= " ?>Html::submitButton('<i class="fas fa-save"></i> Simpan', ['class' => 'btn btn-sm btn-primary']) ?>
        <?= "<?= " ?>Html::Button('<i class="fas fa-times-circle"></i> Batal', ['class' => 'btn btn-sm btn-danger', 'onclick' => 'history.go(-1)']) ?>
    </div>

    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
