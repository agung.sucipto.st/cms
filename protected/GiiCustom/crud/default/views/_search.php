<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->searchModelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="collapse searchBox" id="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search" style="">
  <div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search">

      <?= "<?php " ?>$form = ActiveForm::begin([
          'action' => ['index'],
          'method' => 'get',
  <?php if ($generator->enablePjax): ?>
          'options' => [
              'data-pjax' => 1
          ],
  <?php endif; ?>
      ]); ?>

  <?php
  $count = 0;
  foreach ($generator->getColumnNames() as $attribute) {
      if (++$count < 6) {
          echo "    <?= " . $generator->generateActiveSearchField($attribute) . " ?>\n\n";
      } else {
          echo "    <?php // echo " . $generator->generateActiveSearchField($attribute) . " ?>\n\n";
      }
  }
  ?>
      <div class="form-group">
          <?= "<?= " ?>Html::submitButton('<i class="fas fa-search"></i> Cari', ['class' => 'btn btn-primary btn-sm btn-icon']) ?>
          <?= "<?= " ?>Html::resetButton('<i class="fas fa-sync"></i> Reset', ['class' => 'btn btn-warning btn-sm btn-icon']) ?>
      </div>
      <?= "<?php " ?>ActiveForm::end(); ?>
  </div>
</div>
