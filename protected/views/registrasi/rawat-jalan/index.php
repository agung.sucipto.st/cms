<?php
use yii\helpers\Url;
$this->title = 'Registrasi Rawat Jalan';
?>

<div class="row">
	<div class="col-sm-3">
		<img src="<?= Url::base(true); ?>/themes/stisla/assets/home.png" class="img-fluid"/>
	</div>
	<div class="col-sm-9">
		<h1 style="font-weight: bold">Registrasi Rawat Jalan</h1>

		<i class="lead">The Best Place Way Back</i>
	</div>
</div>
