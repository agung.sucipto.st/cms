<?php
use yii\helpers\Url;
$this->title = 'Home';
?>

<div class="row">
	<div class="col-sm-3">
		<img src="<?= Url::base(true); ?>/themes/stisla/assets/home.png" class="img-fluid"/>
	</div>
	<div class="col-sm-9">
		<h1 style="font-weight: bold">Home</h1>

		<i class="lead">The Best Place Way Back</i>
	</div>
</div>
