<?php
use yii\helpers\Url;
$this->title = 'Core Modul';
?>

<div class="row">
	<div class="col-sm-3">
		<img src="<?= Url::base(true); ?>/themes/stisla/assets/medic.png" class="img-fluid"/>
	</div>
	<div class="col-sm-9">
		<h1 style="font-weight: bold">System Administrator</h1>

		<i class="lead">Manage Aplication Access</i>
	</div>
</div>
